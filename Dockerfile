# Use only in production

FROM tiangolo/uvicorn-gunicorn-fastapi:python3.10

RUN apt-get update && apt-get install ffmpeg libsm6 libxext6 netcat-traditional  -y

COPY pyproject.toml /app
RUN pip3 install poetry
RUN poetry config virtualenvs.create false
RUN poetry install
COPY . /app
