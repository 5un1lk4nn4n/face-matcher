-- Create table for face encoding
CREATE TABLE "face_encodings" ("id" SERIAL PRIMARY KEY,"event_code" varchar(150),"image" VARCHAR(100), "encodings" text);

-- table to manage background tasks
CREATE TABLE "face_background_tasks" ("id" SERIAL PRIMARY KEY,"event_code" varchar(150),"status" VARCHAR(1), "task_name" varchar(150));