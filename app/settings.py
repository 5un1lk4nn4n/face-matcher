from pathlib import Path

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    DATABASE_URL: str
    SECRET:str
    DOMAIN:str


settings = Settings(_env_file=Path(__file__).parent.parent / ".env", _env_file_encoding="utf-8")
