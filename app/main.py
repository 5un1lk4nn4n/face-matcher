import os
from fastapi import FastAPI, BackgroundTasks, Request, File, UploadFile
from typing import List
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from app.schemas import MatchedImageListResponse, MatchedImageRequest, DownloadImageRequest, SelfieImageRequest, \
    SelfieImageResponse, CleanEventRequest
from app.image_processer.main import (face_encoding, match_face_encoding, check_selfie_has_face, clean_event,
                                      upload_event_images
                                      )
from app.db.database import database

app = FastAPI()

origins = [
    "*",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.mount("/images", StaticFiles(directory="images", html=True), name="site")


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.get("/")
async def root():
    return {"message": "Hello World", "cpu_count": os.cpu_count(), "workers": min(32, os.cpu_count() + 4)}


@app.post("/api/matched", response_model=MatchedImageListResponse)
async def match_images(body: MatchedImageRequest):
    return await match_face_encoding(body.selfie_image_url, body.event_code)


@app.post("/api/check-selfie", response_model=SelfieImageResponse)
async def check_selfie(body: SelfieImageRequest):
    return await check_selfie_has_face(body.selfie_image_url)


@app.post("/api/generate-encodings")
async def generate_encodings(request: DownloadImageRequest, background_tasks: BackgroundTasks):
    return await face_encoding(request.event_code, background_tasks)


@app.delete("/api/event/clean")
async def clean_event_after_expiry(body: CleanEventRequest):
    return await clean_event(body.event_code)


@app.post("/api/uploader/large/{event_code}")
async def big_uploader(event_code: str, req: Request, background_tasks: BackgroundTasks):
    return await upload_event_images(event_code, req, None, background_tasks)


@app.post("/api/uploader/multi/{event_code}")
async def multi_uploader(event_code: str, background_tasks: BackgroundTasks, file:  UploadFile = File(...)):
    return await upload_event_images(event_code, None, file, background_tasks)
