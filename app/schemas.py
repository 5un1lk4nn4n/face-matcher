from datetime import datetime
from typing import List
import uuid
from pydantic import BaseModel, EmailStr, constr


class MatchedImageListResponse(BaseModel):
    images: List
    event_code: str
    no_of_images: int
    selfie_image: str
    execution_time: float
    face_detected_in_selfie: int


class MatchedImageRequest(BaseModel):
    selfie_image_url: str
    event_code: str


class SelfieImageRequest(BaseModel):
    selfie_image_url: str

class SelfieImageResponse(BaseModel):
    selfie_image_url: str
    has_face: bool


class DownloadImageRequest(BaseModel):
    remote_drive_url: str
    event_code: str

class CleanEventRequest(BaseModel):
    event_code: str