import os
from urllib.parse import urlparse
import requests


async def download_image(image_url, filepath):
    is_success = False
    if not os.path.exists(filepath):
        r = requests.get(image_url)
        if r.status_code == 200:
            with open(filepath, 'wb') as f:
                f.write(r.content)
                is_success = True
    else:
        is_success = True

    return is_success


async def download_event_image_dir(remote_drive_url, event_code):
    folder = get_event_folder(event_code)


def is_event_folder_available(path):
    return os.path.exists(path)


def get_event_images(image_folder):
    images = []
    for _, _, files in os.walk(image_folder):
        only_images = [image_file for image_file in files if is_image(image_file)]
        images.extend(only_images)
    return images


def get_event_folder(event_code):
    return os.path.join("images", event_code)


async def create_or_get_event_folder(event_code):
    event_folder = get_event_folder(event_code)
    if not is_event_folder_available(event_folder):
        os.makedirs(event_folder)
    return event_folder


def is_image(image_file):
    _, extension = os.path.splitext(image_file)
    return extension.lower() in [".jpg", '.jpeg', '.png']


def get_selfie_image_full_path(selfie_url, folder='./images'):
    selfie_image_name = os.path.basename(urlparse(selfie_url).path)
    selfie_image_path = os.path.join(folder, selfie_image_name)
    return selfie_image_path
