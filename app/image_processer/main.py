import json
import os
import shutil
import time
from typing import List

from fastapi import HTTPException, BackgroundTasks, Request, UploadFile

from app import settings as s
from app.db.database import database
from app.image_processer.compressor import optimize_images
from app.image_processer.downloader import large_upload, multi_upload
from app.image_processer.matcher import get_encoding, matcher, get_faces
from app.image_processer.utils import create_or_get_event_folder, download_image, is_event_folder_available, \
    get_event_folder, get_event_images, get_selfie_image_full_path


async def face_encoding(event_code, background_tasks: BackgroundTasks):
    event_folder = await create_or_get_event_folder(event_code)
    start_time = time.time()
    event_images = get_event_images(event_folder)
    # optimize_images(event_images,event_folder)
    # total_faces=0
    total_faces = await generate_encoding(event_code, event_folder, event_images)
    end_time = time.time()
    execution_time = end_time - start_time
    # once we get all face encodings, compress the size of the images
    background_tasks.add_task(optimize_images, event_images, event_folder)

    return {
        "execution_time": execution_time,
        "total_faces": total_faces
    }


async def generate_encoding(event_code, event_folder, event_images):
    image_encoding_list = []
    for event_image in event_images:
        path = os.path.join(event_folder, event_image)
        # get faces from the images
        faces = await get_faces(path)

        # get encoding for all the faces
        for img_content, img_region, _ in faces[:10]:
            encoding = await get_encoding(img_content)
            image_encoding_list.append({
                "encodings": json.dumps(encoding),
                "image": event_image,
                "event_code": event_code
            })
    query = "INSERT INTO face_encodings(encodings, image,event_code) VALUES (:encodings, :image,:event_code)"
    values = image_encoding_list
    await database.execute_many(query=query, values=values)

    return len(image_encoding_list)


async def match_face_encoding(selfie_url, event_code):
    """
    Compare selfie face encodings against event images and get matched images
    :param:
    :return: matched images with other info
    """
    event_folder = get_event_folder(event_code)
    if not is_event_folder_available(event_folder):
        raise HTTPException(status_code=404, detail="Event Images not available")

    selfie_image_path = get_selfie_image_full_path(selfie_url, event_folder)
    is_downloaded = await download_image(selfie_url, selfie_image_path)
    if not is_downloaded:
        raise HTTPException(status_code=400, detail="Something went wrong with selfie image download")

    start_time = time.time()
    faces = await get_faces(selfie_image_path)

    if len(faces) == 0:
        raise HTTPException(status_code=400, detail="Face not found in Selfie")

    query = f"SELECT * FROM face_encodings where event_code = '{event_code}'"

    event_encodings = await database.fetch_all(query=query)
    if len(event_encodings) == 0:
        raise HTTPException(status_code=404, detail="Event Images not available")

    matched_images = []
    for img_content, img_region, _ in faces:
        encoding = await get_encoding(img_content)
        mi = await matcher(encoding, event_encodings)
        matched_images.extend(mi)
    end_time = time.time()
    execution_time = end_time - start_time
    matched_images = list(set(matched_images))
    matched_images = [f"{s.settings.DOMAIN}images/{event_code}/{img}" for img in matched_images]
    return {
        "images": matched_images,
        "event_code": event_code,
        "no_of_images": len(matched_images),
        "selfie_image": selfie_url,
        "execution_time": execution_time,
        "face_detected_in_selfie": len(faces),
    }


async def check_selfie_has_face(selfie_url):
    selfie_image_path = get_selfie_image_full_path(selfie_url)
    is_downloaded = await download_image(selfie_url, selfie_image_path)
    if not is_downloaded:
        raise HTTPException(status_code=400, detail="Something went wrong with selfie image download")
    faces = await get_faces(selfie_image_path)
    # await has_face_in_image(selfie_image_path)
    has_face = len(faces) > 0
    os.remove(selfie_image_path)
    return {
        "has_face": has_face,
        "selfie_image_url": selfie_url,
    }


async def clean_event(event_code):
    event_folder = get_event_folder(event_code)
    shutil.rmtree(event_folder)
    query = f"Delete from face_encodings where event_code={event_code}"
    database.execute(query)


async def upload_event_images(
        event_code,
        req: Request | None,
        image_files: List[UploadFile] | None,
        background_tasks: BackgroundTasks
):
    response = None
    if req is not None:
        event_images, event_folder, response = await large_upload(event_code, req)

    if image_files is not None:
        event_images, event_folder, response = await multi_upload(event_code, [image_files])
        background_tasks.add_task(generate_encoding, event_code, event_folder, event_images)
        # once we get all face encodings, compress the size of the images
        # background_tasks.add_task(optimize_images, event_images, event_folder)

    return response
