import os
from PIL import Image


class CompressImage:
    COMPRESS_QUALITY = 70
    RESIZE_PERCENT = 25  # reduce percentage

    def __init__(self, image_path, output_dir=None):
        self.image_path = image_path
        self.image = self.get_image()
        self.output_dir = os.path.dirname(image_path) if output_dir is None else output_dir

    def get_output_path(self):
        return os.path.join(self.output_dir, self.get_file_name())

    def get_file_name(self):
        return os.path.basename(self.image_path)

    def resize_image(self):
        reduce_width = (CompressImage.RESIZE_PERCENT / 100) * float(self.image.size[0])
        reduce_height = (CompressImage.RESIZE_PERCENT / 100) * float(self.image.size[1])

        width = float(self.image.size[0]) - reduce_width
        height = float(self.image.size[1]) - reduce_height
        return self.image.resize((int(width), int(height)), Image.Resampling.LANCZOS)

    def get_image(self):
        image = Image.open(self.image_path)

        # # downsize the image with an ANTIALIAS filter (gives the highest quality)
        # image = image.resize((160, 300), Image.ANTIALIAS)
        return image

    def optimize_image(self):
        image = self.resize_image()
        self.image = image
        self.image.save(self.get_output_path(), optimize=True,
                        quality=CompressImage.COMPRESS_QUALITY)  # The saved downsized image size is 22.9kb


def optimize_images(images: list, image_folder: str) -> None:
    for image in images:
        path = os.path.join(image_folder, image)
        optimised = CompressImage(path)
        optimised.optimize_image()
