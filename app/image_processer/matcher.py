from deepface import DeepFace
from deepface.commons import functions, realtime, distance as dst
import json
import numpy as np
import os
from app.image_processer.compressor import CompressImage
AI_MODEL = 'Facenet512'
METRICS = ['cosine', 'euclidean', 'euclidean_l2']
DETECTOR_BACKEND = ['opencv', 'skip', 'mtcnn']
ENFORCE_DETECTION = False
ALIGN = True
NORMALIZATION = "base"


def start_encoding_face(images: list, image_folder: str) -> None:
    for image in images:
        path = os.path.join(image_folder, image)
        optimised = CompressImage(path)
        optimised.optimize_image()


async def get_faces(image_path):
    target_size = functions.find_target_size(model_name=AI_MODEL)
    face_objects = functions.extract_faces(
        img=image_path,
        target_size=target_size,
        detector_backend=DETECTOR_BACKEND[2],
        grayscale=False,
        enforce_detection=ENFORCE_DETECTION,
        align=ALIGN,
    )

    return face_objects


async def matcher(selfie_encoding, event_image_encoding_list):
    images = []
    i = 0
    for event_image_encoding in event_image_encoding_list:

        img_encoding = np.array(json.loads(event_image_encoding.encodings))
        matched = await compare_encoding(
            selfie_encoding,
            img_encoding
        )
        if matched:
            images.append(event_image_encoding.image)
        i += 1
    return images


async def get_encoding(image_path):
    encoding = DeepFace.represent(
        img_path=image_path,
        model_name=AI_MODEL,
        enforce_detection=ENFORCE_DETECTION,
        align=ALIGN,
        # normalization=NORMALIZATION,
        detector_backend=DETECTOR_BACKEND[1],
    )
    return encoding[0]['embedding']


async def compare_encoding(encoding1, encoding2) -> bool:
    distance = dst.findCosineDistance(encoding1, encoding2)
    threshold = dst.findThreshold(AI_MODEL, METRICS[0])
    matched = distance <= threshold
    return matched
